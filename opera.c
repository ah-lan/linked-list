#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};
// Function prototypes to be implemented
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);


// Function to create a new node
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL)
    {
        printf("Memory allocation failed.\n");
        exit(1);
    }
    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

// Function to print the linked list
void printList(struct Node *head)
{
    struct Node *current = head;

    printf("[");
    while (current != NULL)
    {
        printf("%d ", current->number);
        current = current->next;
    }
    printf("]\n");
}

// Function to append a new node at the end of the list
void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);

    if (*head == NULL)
    {
        *head = newNode;
    }
    else
    {
        struct Node *current = *head;
        while (current->next != NULL)
        {
            current = current->next;
        }
        current->next = newNode;
    }
}

// Function to prepend a new node at the beginning of the list
void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

// Function to delete a node by key
void deleteByKey(struct Node **head, int key)
{
    struct Node *current = *head;
    struct Node *prev = NULL;

    while (current != NULL && current->number != key)
    {
        prev = current;
        current = current->next;
    }

    if (current == NULL)
    {
        printf("Key not found in the list.\n");
        return;
    }

    if (prev == NULL)
    {
        *head = current->next;
    }
    else
    {
        prev->next = current->next;
    }

    free(current);
}

// Function to delete a node by value
void deleteByValue(struct Node **head, int value)
{
    struct Node *current = *head;
    struct Node *prev = NULL;

    while (current != NULL && current->number != value)
    {
        prev = current;
        current = current->next;
    }

    if (current == NULL)
    {
        printf("Value not found in the list.\n");
        return;
    }

    if (prev == NULL)
    {
        *head = current->next;
    }
    else
    {
        prev->next = current->next;
    }

    free(current);
}

// Function to insert a new node after a specified key
void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *current = *head;

    while (current != NULL && current->number != key)
    {
        current = current->next;
    }

    if (current == NULL)
    {
        printf("Key not found in the list.\n");
        return;
    }

    struct Node *newNode = createNode(value);
    newNode->next = current->next;
    current->next = newNode;
}

// Function to insert a new node after a specified value
void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *current = *head;

    while (current != NULL && current->number != searchValue)
    {
        current = current->next;
    }

    if (current == NULL)
    {
        printf("Value not found in the list.\n");
        return;
    }

    struct Node *newNode = createNode(newValue);
    newNode->next = current->next;
    current->next = newNode;
}

int main()
{
    struct Node *head = NULL;
    int choice, data, key, searchValue;

    while (1)
    {
        printf("\nLinked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Value\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            printList(head);
            break;
        case 2:
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            break;
        case 3:
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
            break;

        case 4:
            printf("Enter value to delete: ");
            scanf("%d", &data);
            deleteByValue(&head, data);
            break;


        case 5:
            exit(0);
        default:
            printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}
